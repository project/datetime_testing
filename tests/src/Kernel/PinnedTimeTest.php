<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_testing\Kernel;

use Drupal\Component\Datetime\Time;
use Drupal\KernelTests\KernelTestBase;
use Drupal\datetime_testing\TestTime;
use Drupal\datetime_testing\TestTimeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Tests datetime_testing's time by overriding php's native time functions.
 *
 * Test that class behaves in the same way as the regular time class, if not
 * explicitly instructed to behave otherwise, and test its methods for
 * manipulating the time. A class at the bottom of this file overrides php's
 * built-in time methods and pins the current time.
 *
 * @coversDefaultClass \Drupal\datetime_testing\TestTime
 * @group datetime_testing
 *
 * Isolate the tests to prevent side effects from altering system time.
 *
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class PinnedTimeTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'datetime_testing',
  ];

  /**
   * The mocked request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack|\PHPUnit\Framework\MockObject\MockObject
   */
  protected RequestStack|MockObject $requestStack;

  /**
   * The (mocked) normal time class.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $normalTime;

  /**
   * Our testing time class.
   *
   * @var \Drupal\datetime_testing\TestTimeInterface
   */
  protected TestTimeInterface $testTime;

  /**
   * The mock time (unix timestamp).
   *
   * @var int
   */
  protected int $mockTime = 1000000;

  /**
   * The mock time (unix timestamp in microseconds).
   *
   * @var float
   */
  protected float $mockMicroTime = 1000000.10;

  /**
   * The mock request time (unix timestamp).
   *
   * @var int
   */
  protected int $mockRequestTime = 1000000;

  /**
   * The mock request time (unix timestamp in microseconds).
   *
   * @var float
   */
  protected float $mockRequestMicroTime = 1000000.20;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function setUp(): void {
    parent::setUp();

    $this->requestStack = $this->createMock(RequestStack::class);
    $this->normalTime = new Time($this->requestStack);
    $this->testTime = new TestTime($this->normalTime, \Drupal::service('keyvalue'));

    $request = Request::createFromGlobals();
    $request->server->set('REQUEST_TIME', $this->mockRequestTime);
    $request->server->set('REQUEST_TIME_FLOAT', $this->mockRequestMicroTime);

    // Mocks a the request stack getting the current request.
    $this->requestStack->expects($this->any())
      ->method('getCurrentRequest')
      ->willReturn($request);

    $this->testTime->resetTime();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function tearDown(): void {
    $this->testTime->resetTime();
    parent::tearDown();
  }

  /**
   * Make sure the time service is being decorated with TestTime.
   */
  public function testDecoration(): void {
    $service = \Drupal::time();
    $this->assertEquals(TestTime::class, $service::class);
  }

  /**
   * Tests the getRequestTime method.
   *
   * @covers ::getRequestTime
   */
  public function testGetRequestTime(): void {
    $this->assertEquals($this->mockRequestTime, $this->normalTime->getRequestTime());
    $this->assertEquals($this->mockRequestTime, $this->testTime->getRequestTime());
  }

  /**
   * Tests the getRequestMicroTime method.
   *
   * @covers ::getRequestMicroTime
   */
  public function testGetRequestMicroTime(): void {
    $this->assertEqualsWithDelta($this->mockRequestMicroTime, $this->normalTime->getRequestMicroTime(), 0.0001);
    $this->assertEqualsWithDelta($this->mockRequestMicroTime, $this->testTime->getRequestMicroTime(), 0.0001);
  }

  /**
   * Tests the getCurrentTime method.
   *
   * @covers ::getCurrentTime
   */
  public function testGetCurrentTime(): void {
    $this->assertEquals($this->mockTime, $this->normalTime->getCurrentTime());
    $this->assertEquals($this->mockTime, $this->testTime->getCurrentTime());
  }

  /**
   * Tests the getCurrentMicroTime method.
   *
   * @covers ::getCurrentMicroTime
   */
  public function testGetCurrentMicroTime(): void {
    $this->assertEqualsWithDelta($this->mockMicroTime, $this->normalTime->getCurrentMicroTime(), 0.0001);
    $this->assertEqualsWithDelta($this->mockMicroTime, $this->testTime->getCurrentMicroTime(), 0.0001);
  }

  /**
   * Tests the setTime method with whole number of seconds.
   */
  public function testSetTimeInteger(): void {
    $seconds = 2000000;
    $this->testTime->setTime($seconds);
    $this->assertEqualsWithDelta($seconds, $this->testTime->getCurrentMicroTime(), 0.0001);
    $this->assertEquals($seconds, $this->testTime->getCurrentTime());
    $this->assertEqualsWithDelta($seconds + 0.1, $this->testTime->getRequestMicroTime(), 0.0001);
    $this->assertEquals($seconds, $this->testTime->getRequestTime());
  }

  /**
   * Tests the setTime method with fractions of a second.
   */
  public function testSetTimeFloat(): void {
    $seconds = 2000000;
    $milliseconds = 0.85;
    $this->testTime->setTime($seconds + $milliseconds);
    $this->assertEqualsWithDelta($seconds + $milliseconds, $this->testTime->getCurrentMicroTime(), 0.0001);
    // $milliseconds < 1, so do not increment expected time.
    $this->assertEquals($seconds, $this->testTime->getCurrentTime());
    $this->assertEqualsWithDelta($seconds + $milliseconds + 0.1, $this->testTime->getRequestMicroTime(), 0.0001);
    // $milliseconds + 0.1 request time gap < 1, so don't increment the expected
    // time.
    $this->assertEquals($seconds, $this->testTime->getRequestTime());
  }

  /**
   * Tests the setTime method with a string.
   */
  public function testSetTimeString(): void {
    $seconds = 2000000;
    $this->testTime->setTime("1970-01-24 03:33:20 UTC");
    $this->assertEqualsWithDelta($seconds, $this->testTime->getCurrentMicroTime(), 0.0001);
    $this->assertEquals($seconds, $this->testTime->getCurrentTime());
    $this->assertEqualsWithDelta($seconds + 0.1, $this->testTime->getRequestMicroTime(), 0.0001);
    $this->assertEquals($seconds, $this->testTime->getRequestTime());
  }

  /**
   * Tests the shiftTime method with whole number of seconds.
   */
  public function testSetTimeRelative(): void {
    $seconds = 20;
    $this->testTime->setTime("$seconds seconds");
    $this->assertEquals($this->mockTime + $seconds, $this->testTime->getCurrentTime());
    $this->assertEquals($this->mockTime + $seconds, (int) $this->testTime->getCurrentMicroTime());
    $this->assertEquals($this->mockRequestTime + $seconds, $this->testTime->getRequestTime());
    $this->assertEquals($this->mockRequestTime + $seconds, (int) $this->testTime->getRequestMicroTime());
  }

  /**
   * Tests the resetTime method.
   */
  public function testResetTime(): void {
    $this->testTime->setTime(0.5);
    $this->testTime->resetTime();
    $this->assertEqualsWithDelta($this->mockMicroTime, $this->testTime->getCurrentMicroTime(), 0.0001);
    $this->assertEquals($this->mockTime, $this->testTime->getCurrentTime());
    $this->assertEqualsWithDelta($this->mockRequestMicroTime, $this->testTime->getRequestMicroTime(), 0.0001);
    $this->assertEquals($this->mockRequestTime, $this->testTime->getRequestTime());
  }

}

namespace Drupal\Component\Datetime;

/**
 * Shadow time() system call.
 */
function time(): int {
  return 1000000;
}

/**
 * Shadow microtime system call.
 */
function microtime(): float {
  return 1000000.10;
}
