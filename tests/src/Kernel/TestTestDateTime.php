<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_testing\Kernel;

use Drupal\datetime_testing\TestDateTime;

/**
 * A datetime object that allows overriding the default timezone.
 *
 * Intended for testing the TestDatetime class.
 */
class TestTestDateTime extends TestDateTime {

  public function __construct(
    string $time = 'now',
    ?string $timezone = NULL,
    array $settings = [],
    protected string $fallbackTimezone = 'UTC',
  ) {
    parent::__construct($time, $timezone, $settings);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function prepareTimezone(mixed $timezone): \DateTimeZone {
    // Override the default timezone for testing purposes.
    if (empty($timezone) && ($this->fallbackTimezone !== '' && $this->fallbackTimezone !== '0')) {
      return new \DateTimeZone($this->fallbackTimezone);
    }
    else {
      return parent::prepareTimezone($timezone);
    }
  }

}
