<?php

declare(strict_types=1);

namespace Drupal\Tests\datetime_testing\Functional;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Tests\BrowserTestBase;
use Drupal\datetime_testing\TestTime;
use Drush\TestTraits\DrushTestTrait;

/**
 * Tests the datetime test commands.
 *
 * @group datetime_testing
 */
class TestDatetimeTestingCommands extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'datetime_testing',
  ];

  /**
   * Tests the datetime testing get/set command.
   */
  public function testTimeSetAndGet(): void {
    $time_string = '2020-01-15 12:00:00';
    $time = DrupalDateTime::createFromFormat(DateTimePlus::FORMAT, $time_string);
    $this->drush('datetime-testing:set \'' . $time_string . '\'');
    $this->assertErrorOutputEquals(sprintf('[success] Time has been set to \'%s\', timestamp: %d.', $time_string, $time->getTimestamp()));

    $this->drush('datetime-testing:get ');
    $this->assertErrorOutputEquals(sprintf('[success] Current time value is %s, timestamp: %d.', $time_string, $time->getTimestamp()));
  }

  /**
   * Tests the datetime testing freeze command.
   */
  public function testDatetimeFreeze(): void {
    $this->drush('datetime-testing:freeze');
    /** @var \Drupal\Component\Datetime\TimeInterface $time_service */
    $time_service = \Drupal::service('datetime.time');
    $current_time = $time_service->getCurrentTime();
    $this->assertErrorOutputEquals(sprintf('[success] Time is frozen to %s, timestamp: %d.', date(DateTimePlus::FORMAT, $current_time), $current_time));
  }

  /**
   * Tests the datetime testing unfreeze command.
   */
  public function testDatetimeUnfreeze(): void {
    /** @var \Drupal\Component\Datetime\TimeInterface $time_service */
    $time_service = \Drupal::service('datetime.time');
    $current_time = $time_service->getCurrentTime();
    $this->drush('datetime-testing:unfreeze');
    sleep(1);
    $new_time = $time_service->getCurrentTime();
    $this->assertErrorOutputEquals('[success] Time has been unfrozen.');
    $this->assertNotEquals($new_time, $current_time);
  }

  /**
   * Tests the datetime testing reset command.
   */
  public function testDatetimeReset(): void {
    $this->drush('datetime-testing:reset');
    $this->assertErrorOutputEquals('[success] Test time has been reset.');
    $this->assertNull(\Drupal::keyValue(TestTime::DATETIME_TESTING_STORE)->get('datetime_testing.specified_time'));
    $this->assertNull(\Drupal::keyValue(TestTime::DATETIME_TESTING_STORE)->get('datetime_testing.time_started'));
    $this->assertNull(\Drupal::keyValue(TestTime::DATETIME_TESTING_STORE)->get('datetime_testing.time_passing'));
  }

}
