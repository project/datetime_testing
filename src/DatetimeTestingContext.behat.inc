<?php

declare(strict_types=1);

use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\DrupalExtension\Context\DrupalSubContextInterface;

/**
 * A Behat subcontext that allows manipulating the time.
 */
class DatetimeTestingContext extends DrupalSubContextBase implements DrupalSubContextInterface {

  /**
   * Set the time using a string understood by PHP strtotime().
   *
   * @Given the time/date is :time
   * @Given the day is :time
   * @When :time of time pass/passes
   * @When :time pass/passes
   */
  public function timeIs(string $time): void {
    $this->getDriver();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    \Drupal::time()->setTime($time);
  }

  /**
   * Set the time, and freeze it at that time.
   *
   * @Given the time/date is frozen as/at :time
   * @Given the day is frozen as/at :time
   */
  public function timeIsFrozenAs(string $time): void {
    $this->getDriver();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    \Drupal::time()->freezeTime();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    \Drupal::time()->setTime($time);
  }

  /**
   * Stop time from passing.
   *
   * @Given time is frozen
   */
  public function timeIsFrozen(): void {
    $this->getDriver();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    \Drupal::time()->freezeTime();
  }

  /**
   * Allow time to resume passing.
   *
   * @When time is unfrozen
   */
  public function timeIsUnfrozen(): void {
    $this->getDriver();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    \Drupal::time()->unfreezeTime();
  }

  /**
   * Undo the effect of any time manipulations.
   *
   * @AfterScenario
   */
  public function resetTime(AfterScenarioScope $scope): void {
    $this->getDriver();
    // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
    if (\Drupal::moduleHandler()->moduleExists('datetime_testing')) {
      // @phpstan-ignore-next-line \Drupal calls should be avoided in classes; subcontext doesn't support DI
      \Drupal::time()->resetTime();
    }
  }

}
