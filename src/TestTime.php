<?php

declare(strict_types=1);

namespace Drupal\datetime_testing;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;

/**
 * {@inheritdoc}
 */
class TestTime implements TestTimeInterface {

  use DependencySerializationTrait;

  /**
   * The key value service for this module.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValueStore;

  const DATETIME_TESTING_STORE = 'datetime_testing';

  public function __construct(
    protected TimeInterface $realTime,
    KeyValueFactoryInterface $key_value_factory,
  ) {
    $this->keyValueStore = $key_value_factory->get(self::DATETIME_TESTING_STORE);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function setTime(int|float|string $time): void {
    if (!is_string($time) && !is_int($time) && !is_float($time)) {
      throw new \Exception('Time to be set must be passed as a string, integer or float.');
    }
    if (is_string($time)) {
      $settings = ['current_time' => $this->getCurrentTime()];
      $timeObject = new TestDateTime($time, NULL, $settings);
      $time = $timeObject->getTimestamp();
    }

    $this->keyValueStore->set('datetime_testing.specified_time', $time);
    if ($this->keyValueStore->get('datetime_testing.time_passing') !== FALSE) {
      $this->keyValueStore
        ->set('datetime_testing.time_started', $this->realTime->getCurrentMicroTime());
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function resetTime(): void {
    $this->keyValueStore->set('datetime_testing.specified_time', NULL);
    $this->keyValueStore->set('datetime_testing.time_started', NULL);
    $this->keyValueStore->set('datetime_testing.time_passing', NULL);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function freezeTime(): void {
    // Do nothing if time already frozen.
    if ($this->keyValueStore->get('datetime_testing.time_passing') !== FALSE) {
      $this->setTime($this->getCurrentMicroTime());
      $this->keyValueStore->set('datetime_testing.time_passing', FALSE);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function unfreezeTime(): void {
    // Do nothing if time not frozen.
    // If time_passing is null, don't set a start time.
    if ($this->keyValueStore->get('datetime_testing.time_passing') === FALSE) {
      $this->keyValueStore->set('datetime_testing.time_passing', TRUE);
      $this->keyValueStore->set('datetime_testing.time_started', $this->realTime->getCurrentMicroTime());
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getCurrentMicroTime(): float {
    $specifiedTime = $this->keyValueStore->get('datetime_testing.specified_time');
    $baseTime = empty($specifiedTime) ? $this->realTime->getCurrentMicroTime() : $specifiedTime;
    $passed = $this->getMicroTimePassed();
    $timeNow = $baseTime + $passed;
    return $timeNow;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getCurrentTime(): int {
    return (int) $this->getCurrentMicroTime();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getRequestMicroTime(): float {
    $requestLag = $this->realTime->getCurrentMicroTime() - $this->realTime->getRequestMicroTime();
    return $this->getCurrentMicroTime() - $requestLag;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getRequestTime(): int {
    // In theory, we should be able to use getRequestMicroTime to get the same
    // result but getting micro time on kernel tests is not possible at the
    // moment. See https://www.drupal.org/project/drupal/issues/3168449
    // for more info.
    $requestLag = $this->realTime->getCurrentTime() - $this->realTime->getRequestTime();
    return $this->getCurrentTime() - $requestLag;
  }

  /**
   * Get how much time has passed since it began to be allowed to flow freely.
   *
   * @return float
   *   How many seconds have passed.
   */
  protected function getMicroTimePassed(): float {
    $start = $this->keyValueStore->get('datetime_testing.time_started');
    if (($this->keyValueStore->get('datetime_testing.time_passing') !== FALSE) && !empty($start)) {
      $now = $this->realTime->getCurrentMicroTime();
      return $now - $start;
    }
    return 0;
  }

}
