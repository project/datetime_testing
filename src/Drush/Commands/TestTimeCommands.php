<?php

declare(strict_types=1);

namespace Drupal\datetime_testing\Drush\Commands;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide Drush commands for the test time service.
 */
class TestTimeCommands extends DrushCommands {

  public function __construct(
    protected TimeInterface $testTime,
  ) {
    parent::__construct();
  }

  /**
   * DI for the Drush command.
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('datetime.time')
    );
  }

  /**
   * Set test time.
   *
   * @param string $time
   *   Date and time to be set, in the following format 'Y-m-d H:i:s'.
   * @param ?string $timezone
   *   The optional timezone in string format ('Europe/Paris'). Defaults to UTC.
   *
   * @usage datetime-testing:set '2020-01-15 12:00:00'
   *
   * @command datetime-testing:set
   */
  public function set(string $time, ?string $timezone = NULL): void {
    $drupal_datetime = DrupalDateTime::createFromFormat(DateTimePlus::FORMAT, $time, $timezone);
    $timestamp = $drupal_datetime->getTimestamp();
    $this->testTime->setTime($timestamp);
    $logged_timezone = $drupal_datetime->getTimezone()->getName();
    $this->logger()->success("Time has been set to '$time', timestamp: $timestamp, timezone: $logged_timezone.");
  }

  /**
   * Get current time.
   *
   * @usage datetime-testing:get
   *
   * @command datetime-testing:get
   */
  public function get(): void {
    $time = $this->testTime->getCurrentTime();
    $drupal_datetime = DrupalDateTime::createFromTimestamp($time);
    $date = $drupal_datetime->format(DateTimePlus::FORMAT);
    $logged_timezone = $drupal_datetime->getTimezone()->getName();
    $this->logger()->success("Current time value is $date, timestamp: $time, timezone: $logged_timezone.");
  }

  /**
   * Freeze test time.
   *
   * @command datetime-testing:freeze
   */
  public function freeze(): void {
    $this->testTime->freezeTime();
    $time = $this->testTime->getCurrentTime();
    $drupal_datetime = DrupalDateTime::createFromTimestamp($time);
    $date = $drupal_datetime->format(DrupalDateTime::FORMAT);
    $logged_timezone = $drupal_datetime->getTimezone()->getName();
    $this->logger()->success("Time is frozen to $date, timestamp: $time, timezone: $logged_timezone.");
  }

  /**
   * Unfreeze test time.
   *
   * @command datetime-testing:unfreeze
   */
  public function unfreeze(): void {
    $this->testTime->freezeTime();
    $this->logger()->success("Time has been unfrozen.");
  }

  /**
   * Reset test time.
   *
   * @usage datetime-testing:reset
   *
   * @command datetime-testing:reset
   */
  public function reset(): void {
    $this->testTime->resetTime();
    $this->logger()->success("Test time has been reset.");
  }

}
