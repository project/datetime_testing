CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Usage
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Datetime Testing module provides an API that helps develop automated
tests of Drupal functionality that depends on dates and times.

The module provides the following:

 * Drupal's datetime.time service (which informs Drupal classes about the
   current time) is decorated, adding functions that allow the reported time to
   be altered or frozen.
 * A drop-in replacement for DrupalDateTime (and thus PHP's \Datetime),
   which respects the current time reported by datetime.time when
   interpreting strings as datetime objects.
 * A subcontext for the Drupal Extension for Behat, allowing scenario steps like
   `Given the time is 12pm` or `When "1 hour" passes`.

This is particularly helpful when writing functional tests because the decorated
time service uses Drupal's keyvalue service to persist manipulated times across
requests. Merely mocking the time service is sufficient for unit tests but not
for functional tests.


USAGE
------------

This is an API-module for developers; it has no useful effect without custom
code.

```

// Specify the time.
\Drupal::time()->setTime('2008-12-03 09:15pm');
// Returns a timestamp equivalent to '2008-12-03 09:15pm'.
echo \Drupal::time()->getCurrentTime();

// Stop time from flowing.
\Drupal::time()->freezeTime();
\Drupal::time()->setTime('2008-12-03 09:15pm');
sleep(2);
// Still returns a timestamp equivalent to '2008-12-03 09:15pm'.
echo \Drupal::time()->getCurrentTime();

// Specify a new time using anything understood by PHP's strtotime().
// Note that setting the time, even relatively, discards any fractional second
// known for the current time.
\Drupal::time()->setTime('+1 hour);

// The same string parsing logic is available in your own objects.
$date = new TestDateTime('+1 hour');

// This will be true.
echo ($date->getTimestamp() === \Drupal::time()->getCurrentTime());

// Let's allow time to flow again.
\Drupal::time()->unFreezeTime();
sleep(60);
// Now returns a timestamp equivalent to '2008-12-03 09:16pm'.
echo \Drupal::time()->getCurrentTime();

// Return to the normal behavior of datetime.time.
\Drupal::time()->resetTime();
```

For further API documentation, see \Drupal\datetime_testing\TestTimeInterface
and other classes in the module.

For examples of Behat step syntax, see features/time.feature.


REQUIREMENTS
------------

Requires Drupal 10+.

Older versions of the module support Drupal versions as old as 8.3.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.

This module is for testing purposes only. It should not be installed on
production sites as it may slow performance.


CONFIGURATION
-------------

In order to use the Behat steps in the provided subcontext, indicate the path
to search for the subcontext in your project's behat.yml file:

```
Drupal\DrupalExtension:
  subcontexts:
    paths:
      - "/app/web/modules/contrib/datetime_testing"
```

You do not need to declare the subcontext under the `contexts` key of behat.yml.


DRUSH COMMANDS
--------------

The module provides the following Drush commands:

- `datetime-testing:set`: Set the test time. Format: `Y-m-d H:i:s`.
- `datetime-testing:get`: Get the current time.
- `datetime-testing:freeze`: Freeze time.
- `datetime-testing:unfreeze`: Unfreeze time.
- `datetime-testing:reset`: Reset to true current time (unfreezes if frozen).


MAINTAINERS
-----------

Current maintainer:
 * Patrick Kenny (ptmkenny) - https://drupal.org/u/ptmkenny

Original author and maintainer:
 * Jonathan Shaw (jonathanshaw) - https://drupal.org/u/jonathanshaw

This project has been sponsored by:
 * Awakened Heart Sangha: A UK Buddhist community. Visit https://www.ahs.org.uk.
